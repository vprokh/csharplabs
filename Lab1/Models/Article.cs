using System;
using Lab1.Interfaces;

namespace Lab1.Models
{
    [Serializable]
    public class Article : IRateAndCopy
    {
        public Person Author { get; set; }
        public string Name { get; set; }
        public double Rating { get; set; }

        public Article() : this (new Person(), "Article name", 0.0)
        {
        }

        public Article(Person author, string name, double rating)
        {
            Author = author;
            Name = name;
            Rating = rating;
        }

        public override string ToString()
        {
            return string.Format("Author: {0}, Article name: {1}, Rating: {2}", Author, Name, Rating);
        }

        protected bool Equals(Article other)
        {
            return Equals(Author, other.Author)
                   && string.Equals(Name, other.Name)
                   && Rating.Equals(other.Rating);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            
            return obj.GetType() == GetType() && Equals((Article) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Author != null ? Author.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Rating.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Article left, Article right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Article left, Article right)
        {
            return !Equals(left, right);
        }
        
        public object DeepCopy()
        {
            var article = (Article) MemberwiseClone();

            article.Author = (Person) Author.DeepCopy();

            return article;
        }
    }
}