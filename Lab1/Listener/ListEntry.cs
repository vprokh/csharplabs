namespace Lab1.Listener
{
    public class ListEntry
    {
        public string CollectionName { get; set; }
        public string EventType { get; set; }
        public int ChangedElementNumber { get; set; }

        public ListEntry() : this(null, null, 0)
        {
        }

        public ListEntry(string collectionName, string eventType, int elementNumber)
        {
            CollectionName = collectionName;
            EventType = eventType;
            ChangedElementNumber = elementNumber;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}",
                CollectionName, EventType, ChangedElementNumber);
        }
    }
}