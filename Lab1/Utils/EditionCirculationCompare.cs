using System.Collections.Generic;
using Lab1.Models;

namespace Lab1.Utils
{
    public class EditionCirculationCompare : IComparer<Edition>
    {
        public int Compare(Edition x, Edition y)
        {
            if (x != null && y != null)
            {
                return x.Circulation.CompareTo(y.Circulation);
            }

            return 0;
        }
    }
}