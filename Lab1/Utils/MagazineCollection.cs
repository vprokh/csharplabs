using System.Collections.Generic;
using System.Linq;
using Lab1.Events;
using Lab1.Models;

namespace Lab1.Utils
{
    public class MagazineCollection
    {
        public delegate void MagazineListHandler(object source, MagazineListHandlerEventArgs args);

        public string CollectionName { get; set; }
        public List<Magazine> Magazines { get; set; }
        public event MagazineListHandler MagazineAdded;
        public event MagazineListHandler MagazineReplaced;

        public bool Replace(int replacedIndex, Magazine magazine)
        {
            if (Magazines.Count <= replacedIndex) return false;

            Magazines[replacedIndex] = magazine;
            if (MagazineReplaced != null)
            {
                MagazineReplaced.Invoke(
                    this,
                    new MagazineListHandlerEventArgs(CollectionName, "Replacing", replacedIndex)
                );
            }

            return true;
        }

        public Magazine this[int index]
        {
            get { return Magazines[index]; }
            set
            {
                Magazines[index] = value;
                if (MagazineReplaced != null)
                {
                    MagazineReplaced.Invoke(
                        this,
                        new MagazineListHandlerEventArgs(CollectionName, "Replacing", index)
                    );
                }
            }
        }

        public MagazineCollection() : this(new List<Magazine>())
        {
        }

        public MagazineCollection(List<Magazine> magazines)
        {
            Magazines = magazines;
        }

        public IEnumerable<Magazine> MagazinesWithMonthlyFrequency
        {
            get { return Magazines.Where(x => x.OutFrequency == Frequency.Monthly); }
        }

        public double MaxArticleAverageRating
        {
            get { return Magazines.Count != 0 ? Magazines.Select(x => x.ArticlesAverageRating).Max() : 0; }
        }

        public void AddDefaults()
        {
            Magazines.AddRange(new Magazine[]
            {
                TestCollections.GenerateMagazineWithIndex(1), TestCollections.GenerateMagazineWithIndex(2),
                TestCollections.GenerateMagazineWithIndex(3)
            });

            if (MagazineAdded == null) return;
            MagazineAdded.Invoke(this,
                new MagazineListHandlerEventArgs(CollectionName, "Adding",
                    Magazines.Count - 3));
            MagazineAdded.Invoke(this,
                new MagazineListHandlerEventArgs(CollectionName, "Adding",
                    Magazines.Count - 2));
            MagazineAdded.Invoke(this,
                new MagazineListHandlerEventArgs(CollectionName, "Adding",
                    Magazines.Count - 1));
        }

        public void AddMagazines(params Magazine[] magazines)
        {
            foreach (var magazine in magazines)
            {
                Magazines.Add(magazine);
                if (MagazineAdded != null)
                {
                    MagazineAdded.Invoke(this,
                        new MagazineListHandlerEventArgs(CollectionName, "Adding", Magazines.Count - 1));
                }
            }
        }

        public void SortByName()
        {
            Magazines.Sort();
        }

        public void SortByDate()
        {
            Magazines.Sort(new Edition().Compare);
        }

        public void SortByCirculation()
        {
            Magazines.Sort(new EditionCirculationCompare().Compare);
        }

        public List<Magazine> GetMagazinesWithSpecificAverageRating(double rating)
        {
            return Magazines.Where(x => x.ArticlesAverageRating >= rating).ToList();
        }

        public override string ToString()
        {
            return string.Format("Magazines:\n{0}", string.Join("\n", Magazines.Select(x => x.ToString()).ToArray()));
        }

        public virtual string ToShortString()
        {
            return string.Format("Magazines:\n{0}",
                string.Join("\n", Magazines.Select(x => x.ToShortString()).ToArray()));
        }
    }
}