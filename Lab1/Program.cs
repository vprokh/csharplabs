﻿using System;
using System.IO;
using System.Threading;
using Lab1.Utils;

namespace Lab1
{
    class Program
    {
        private const string Path = @"E:\labs\php";
        
        static void Main(string[] args)
        {
            try
            {
                new DirectoryUtil(Path).GetAllFilesSize();
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine(e);
            }

            new Thread(Watcher.Watch).Start();

            Console.Read();
        }
    }
}